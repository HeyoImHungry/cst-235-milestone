package beans;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class Order {
	//establishing variables
	String orderNo = "";
	String jelloFlavor = "";
	String jelloSize = "";
	float price = 0;
	int quantity = 0;
	
	//default constructor
	public Order() {}
	
	//sets Order to whatever the variables are set to
	public Order(String orderNo, String jelloFlavor, String jelloSize, float price, int quantity) {
		this.orderNo = orderNo;
		this.jelloFlavor = jelloFlavor;
		this.jelloSize = jelloSize;
		this.price = price;
		this.quantity = quantity;
	}
	
	//getters and setters
	public String getJelloSize() {
		return jelloSize;
	}

	public void setJelloSize(String jelloSize) {
		this.jelloSize = jelloSize;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getJelloFlavor() {
		return jelloFlavor;
	}

	public void setJelloFlavor(String jelloFlavor) {
		this.jelloFlavor = jelloFlavor;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}