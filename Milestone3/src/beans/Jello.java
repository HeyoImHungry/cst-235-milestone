package beans;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class Jello {
	List <Order> orders = new ArrayList<Order>();

	//creates a dummy list of orders
	public Jello() {
		 orders.add(new Order("0000000000", "Strawberry", "S", (float)1.00, 1));
		 orders.add(new Order("0000000001", "Lime", "M", (float)1.10, 1));
		 orders.add(new Order("0000000002", "Cherry", "L", (float)1.20, 1));
		 orders.add(new Order("0000000003", "Banana", "XL", (float)1.30, 1));
		 orders.add(new Order("0000000004", "Kiwi", "S", (float)1.40, 1));
		 orders.add(new Order("0000000005", "Watermelon", "M", (float)1.50, 1));
		 orders.add(new Order("0000000006", "Mango", "L", (float)1.60, 1));
		 orders.add(new Order("0000000007", "Pineapple", "XL", (float)1.70, 1));
		 orders.add(new Order("0000000008", "Orange", "S", (float)1.80, 1));
		 orders.add(new Order("0000000009", "Grapefruit", "M", (float)1.90, 1));
	 }
	//getter and setter for Order array list
	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
}