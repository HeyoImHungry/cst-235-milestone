//Noah Canepa
package controllers;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import beans.User;
import business.LoginBusinessInterface;

@ManagedBean
public class FormController {
	
	//injects the EJB for user login
	@Inject
	LoginBusinessInterface service;
	
	//sign in and Register redirect
	public String onSubmit() {
		service.test();
		//retrieves user values
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		//put user into Post request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		//show next page
		return "Response.xhtml";
	}
	
	//goes to register page
	public String onRegister() {
		//retrieves user values
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		//put user into Post request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		//show next page
		return "Register.xhtml";
	}
	
	//Redirects from Register to finish
	public String onFinish() {
		//retrieves user values
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		//put user into Post request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		//show next page
		return "Finish.xhtml";
	}
	
	//Allows the user to sign out of the program from the response page
	public String onLoginAgain() {
		//retrieves user values
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		//put user into Post request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		//show next page
		return "login-form.xhtml";
	}
	
	//Sends from the response Page to add product page
	public String onAddProduct() {
		//retrieves user values
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		//put user into Post request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		//show next page
		return "AddProduct.xhtml";
	}
	
	//after completing add product, sends to page to show what they completed
	public String onFinishAddingProduct() {
		//retrieves user values
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		//put user into Post request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		//show next page
		return "FinishAddingProduct.xhtml";
	}
	
	//sends back to the Response page, but doesnt display the user signed in
	public String onReturnToOrders() {
		//retrieves user values
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		//put user into Post request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		//show next page
		return "Response.xhtml";
	}
}