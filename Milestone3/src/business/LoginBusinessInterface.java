package business;

import javax.ejb.Local;

@Local
/*
 * this is the interface used for the EJB that lets the program know that the user has
 * successfully signed in, currently only prints to the console
 */
public interface LoginBusinessInterface {
	public void test();
}