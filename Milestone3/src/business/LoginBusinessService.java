package business;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

/**
 * Session Bean implementation class OrdersBusinessService
 */
@Stateless
@Local(LoginBusinessInterface.class)
@Alternative
public class LoginBusinessService implements LoginBusinessInterface {
//when the user either signs in or registers a message is displayed in the console
    public void test() {
        System.out.println("User Successfully logged in");
    }

}