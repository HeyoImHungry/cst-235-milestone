package Models;

public class Product {
	int productID;
	int userID;
	String PTitle = "";
	String PAuthor = "";
	String PGenre = "";
	String PDate = "";
	String PDesc = "";
	
	public Product() {}
	
	public Product(String PTitle, String PAuthor, String PGenre, String PDate, String PDesc) {
		super();
		this.PTitle = PTitle;
		this.PAuthor = PAuthor;
		this.PGenre = PGenre;
		this.PDate = PDate;
		this.PDesc = PDesc;
	}

	public void setPTitle(String pTitle) {
		PTitle = pTitle;
	}

	public String getPAuthor() {
		return PAuthor;
	}

	public void setPAuthor(String pAuthor) {
		PAuthor = pAuthor;
	}

	public String getPGenre() {
		return PGenre;
	}

	public void setPGenre(String pGenre) {
		PGenre = pGenre;
	}

	public String getPDate() {
		return PDate;
	}

	public void setPDate(String pDate) {
		PDate = pDate;
	}

	public String getPDesc() {
		return PDesc;
	}

	public void setPDesc(String pDesc) {
		PDesc = pDesc;
	}
}
