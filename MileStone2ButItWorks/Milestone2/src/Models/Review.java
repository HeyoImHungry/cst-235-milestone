package Models;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class Review {
	int reviewID;
	int userID;
	int productID;
	String content = "";
	
	public Review() {}
	
	public Review(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
