package Models;

public class Product {
	int productID;
	int userID;
	String PTitle = "";
	String PAuthor = "";
	String PGenre = "";
	String PDate = "";
	String PDesc = "";
	
	public Product() {}
	
	public Product(String PTitle, String PAuthor, String PGenre, String PDate, String PDesc) {
		this.PTitle = PTitle;
		this.PAuthor = PAuthor;
		this.PGenre = PGenre;
		this.PDate = PDate;
		this.PDesc = PDesc;
	}
	public String getPTitle() {
		return PTitle;
	}
	public void setPTitle(String PTitle) {
		this.PTitle = PTitle;
	}

	public String getPAuthor() {
		return PAuthor;
	}

	public void setPAuthor(String PAuthor) {
		this.PAuthor = PAuthor;
	}

	public String getPGenre() {
		return PGenre;
	}

	public void setPGenre(String PGenre) {
		this.PGenre = PGenre;
	}

	public String getPDate() {
		return PDate;
	}

	public void setPDate(String PDate) {
		this.PDate = PDate;
	}

	public String getPDesc() {
		return PDesc;
	}

	public void setPDesc(String PDesc) {
		this.PDesc = PDesc;
	}
}
