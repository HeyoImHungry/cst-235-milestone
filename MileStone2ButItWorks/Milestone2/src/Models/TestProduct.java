package Models;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


@ManagedBean
@ViewScoped
public class TestProduct {
		List<Product> products = new ArrayList<Product>();
		
		public TestProduct() {
			products.add(new Product("Title1", "Author1", "Genre1", "today1", "describe1"));
			products.add(new Product("Title2", "Author2", "Genre2", "today2", "describe2"));
			products.add(new Product("Title3", "Author3", "Genre3", "today3", "describe3"));
			products.add(new Product("Title4", "Author4", "Genre4", "today4", "describe4"));
			products.add(new Product("Title5", "Author5", "Genre5", "today5", "describe5"));
		}

		public List<Product> getTestProduct() {
			return products;
		}

		public void setTestProduct(List<Product> products) {
			this.products = products;
		}

}
