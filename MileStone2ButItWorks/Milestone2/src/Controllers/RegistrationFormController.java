package Controllers;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import Models.User;

@ManagedBean
public class RegistrationFormController {
	public String onSubmit() {
		
			FacesContext context = FacesContext.getCurrentInstance();
			User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
			return "book.xhtml";
	}
}
