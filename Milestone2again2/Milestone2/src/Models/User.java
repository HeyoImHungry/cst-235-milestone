package Models;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ManagedBean
@ViewScoped
public class User {
	
	//TODO: Jordan change min and max to whatever was in your database
	int userID;
			
	@NotNull(message = "Please enter a First Name. This is a required field.")
	@Size(min=2, max=15)
	String firstName = "";
	
	@NotNull(message = "Please enter a Last Name. This is a required field.")
	@Size(min=4, max=15)
	String lastName = "";
	
	@NotNull(message = "Please enter a Email Name. This is a required field.")
	@Size(min=4, max=15)
	String email ="";
	
	@NotNull(message = "Please enter a Username. This is a required field.")
	@Size(min=4, max=15)
	String userName = "";
	
	@NotNull(message = "Please enter a Password. This is a required field.")
	@Size(min=7, max=25)
	String password = "";
	
	@NotNull(message = "Please enter a phoneNumeber. This is a required field.")
	@Size(min=4, max=15)
	String phoneNumber="";
	
	public User() {}
	
	
	public User(String firstName, String lastName, String email,
			String userName, String password, String phoneNumber) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.userName = userName;
		this.password = password;
		this.phoneNumber = phoneNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
