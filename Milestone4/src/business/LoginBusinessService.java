package business;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;

import beans.Order;
import beans.User;
import data.DataAccessInterface;

/**
 * Session Bean implementation class LoginBusinessService
 */
@Stateless
@Local(LoginBusinessInterface.class)
@Alternative
public class LoginBusinessService implements LoginBusinessInterface {
	@Inject
	DataAccessInterface<Order> orderDataService;
	
	public LoginBusinessService() {}
	
	//when the user either signs in or registers a message is displayed in the console
    public void test() {
        System.out.println("User Successfully logged in");
    }
    
    //gets all the orders from the database
    public List<Order> getOrders() {
		return orderDataService.findAll();
	}
    //creates an order and adds it to the database
    public boolean createOrder(Order order) {
    	orderDataService.create(order);
		return true;
	}
    
    //deletes an Order From the database
    public boolean deleteOrder(int x) {
    	orderDataService.delete(x);
		return true;
	}
    
    //creates a user and adds them to the database
    public boolean createUser(User user) {
    	orderDataService.createUser(user);
		return true;
	}
    
	@Override
	public void setOrders(List<Order> orders) {
		// TODO Auto-generated method stub
		
	}

}