package business;

import java.util.List;

import javax.ejb.Local;

import beans.Order;
import beans.User;

@Local
/**
 * this is the interface used to define methods to implement different business logic throughout the program
 */
public interface LoginBusinessInterface {
	//displays when the user logs in
	public void test();
	//retrieves list of order
	public List<Order> getOrders();
	//adds a new order to the database
	public boolean createOrder(Order order);
	//adds a new user to the database
	public boolean createUser(User user);
	public void setOrders(List<Order> orders);
	//deletes a order from the database
	public boolean deleteOrder(int x);
}