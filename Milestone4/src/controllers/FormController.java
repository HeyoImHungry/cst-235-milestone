//Noah Canepa
package controllers;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import beans.Order;
import beans.User;
import business.LoginBusinessInterface;
/**
 * mostly just redirects users from page to page
 * also has instances of implementing business logic when certain buttons are pressed
 * such as adding orders or users to the database
 */
@ManagedBean
public class FormController {
	
	//injects the EJB for user login
	@Inject
	LoginBusinessInterface service;
	
	//sign in and Register redirect
	public String onSubmit() {
		service.test();
		//retrieves user values
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		//put user into Post request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		//show next page
		return "Response.xhtml";
	}
	
	//goes to register page
	public String onRegister() {
		//retrieves user values
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		//put user into Post request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		//show next page
		return "Register.xhtml";
	}
	
	//Redirects from Register to finish
	public String onFinish(User user) {
		service.createUser(user);
		//put user into Post request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		//show next page
		return "Finish.xhtml";
	}
	
	//Redirects from Response to Deletion Page
	public String onDeleteProduct() {
		//show next page
		return "DeleteProduct.xhtml";
	}
	
	//Redirects from Response to Deletion Page
	public String onConfirmDeletion() {
		//show next page
		return "ConfirmDeletion.xhtml";
	}
	
	public String onDelete(int x) {
		//deletes the selected ID from the database
		service.deleteOrder(x);
		//show next page
		return "Response.xhtml";
	}
	
	//Allows the user to sign out of the program from the response page
	public String onLoginAgain() {
		//retrieves user values
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		//put user into Post request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		//show next page
		return "login-form.xhtml";
	}
	
	//Sends from the response Page to add product page
	public String onAddProduct() {
		//retrieves user values
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		//put user into Post request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		//show next page
		return "AddProduct.xhtml";
	}
	
	//sends back to the Response page, but doesn't display the user signed in
	public String onReturnToOrders() {
		//retrieves user values
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		//put user into Post request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		//show next page
		return "Response.xhtml";
	}
	//gets the information from the user then uploads it to the order database
	public String onConfirmOrder(Order order) {
		//retrieves user values
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		//put user into Post request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		//show adds order to database
		service.createOrder(order);
		//show next page
		return "Response.xhtml";
	}
	//allows for use of business logic
	public LoginBusinessInterface getService() {
		return service;
	}
}